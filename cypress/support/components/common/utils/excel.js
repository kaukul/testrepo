
var excelData
var jsonString

Given(/^File named "([^"]*)" is opened$/, (filename) => {
	const data = [
		"username",
		"password"
	];

	cy.parseXlsx("example.xlsx").then(jsonData => {
		jsonString = JSON.stringify(jsonData);
	});
});

Given(/^User can search data in "([^"]*)" in "([^"]*)" in "([^"]*)" "([^"]*)" to be "([^"]*)"$/, (file, sheet, row, column, value) => {
	excelData = JSON.parse(jsonString)
	for (let index = 0; index < excelData.length; index++) {
		if (excelData[index].name == sheet) {
			expect(excelData[index].data[row][column - 1]).to.eqls(value);
		}
	}
});

Given(/^Test site is open$/, () => {
	cy.visit("https://www.tricentis.com/qtest-login/");
});

var username
var data

When(/^User picks username from "([^"]*)"$/, (sheet) => {
	excelData = JSON.parse(jsonString)
	for (let index = 0; index < excelData.length; index++) {
		if (excelData[index].name == sheet) {
			username = excelData[index].data[1][0]
		}
	}
});

Then(/^User can login$/, () => {
	cy.get('.team-domain').type(username);
});

When(/^user searches data on screen$/, () => {
	excelData = JSON.parse(jsonString)
	// excelData[sheetNo].data[row][column]
	data = excelData[0].data[1][1]
	cy.contains(data);
});