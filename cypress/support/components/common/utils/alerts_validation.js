import { Given, And, Then, When, Before } from "cypress-cucumber-preprocessor/steps"

Given(/^User should validate alert popup$/, () => {
  cy.visit("https://register.rediff.com/register/register.php?FormName=user_details");
  cy.get('input[type="submit"]').click();
  cy.on('window:alert', (txt) => {
    expect(txt).to.contains('Your full name cannot be blank.\nPlease enter your firstname and lastname e.g. Sameer Bhagwat');
  })

});
And(/^user should validate confirm pop-up$/, () => {
  cy.visit("http://testautomationpractice.blogspot.com/");
  cy.get('#HTML9 > div.widget-content > button').click();
  cy.on("window:confirm", (txt) => {
    expect(txt).to.equal("Press a button!");
  });
});
Then(/^user should validate close the confirm pop-up$/, () => {
  cy.visit("http://testautomationpractice.blogspot.com/");
  cy.on("window:confirm", (txt) => {
    return false;
    expect(txt).to.equal("Press a button!");
  });
  cy.get('[name="confirmation"]').click();
});