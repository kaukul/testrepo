/// <reference types="cypress" />

/**
 * Home Page object has methods to handle elements of the home page
 *  
 */

const homePagePF = require('../page_factory/home_page_pf.json')

class HomePage {

    /**
     * @returns Title of the Home Page
     */
    validateTitle() {
        cy.title().should('eq', homePagePF.title)
    }
}

export default HomePage