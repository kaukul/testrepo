/// <reference types="cypress" />

/**
 * Login Page object has methods to login into application 
 *  
 */


const loginPagePF = require('../page_factory/login_page_pf.json')
var field

class LoginPage {
    constructor() {
        this.txtuserName = loginPagePF.txtUserName
        this.txtpassword = loginPagePF.txtPassword
        this.Btnfield = loginPagePF.btnSignIn
    }

    /**
     * 
     * @param {string} URL url of the application
     */
    visit(URL) {
        cy.visit(URL)
    }

    /**
     * This method sets value in username field
     * 
     * @param {string} value username
     */
    setUsername(value) {
        field = cy.get(this.txtuserName)
        field.clear()
        field.type(value)
    }

    /**
     * This method sets value in password field
     * 
     * @param {string} value password
     */
    setPassword(value) {
        field = cy.get(this.txtpassword)
        field.clear()
        field.type(value)
    }

    /**
     * This method clicks on SignIn button
     * 
     */
    clickSignIn() {
        field = cy.get(this.Btnfield)
        field.click();
    }
}

export default LoginPage