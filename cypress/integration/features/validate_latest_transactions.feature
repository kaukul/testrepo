Feature: Verify transactions on applitools website

        This feature file covers transaction verification scenarios for a demo website..
        Demo website - https://demo.applitools.com/...

        Scenario: Verify user is able to see Starbucks transaction
                Given User logs in into applitools website
                When User clicks on View Statement
                Then Starbucks transaction should be present

        Scenario: Verify user is able to see Amazon transaction
                Given User logs in into applitools website
                When User clicks on View Statement
                Then Amazon transaction should be present

        Scenario Outline: Verify user is able to view following transactions
                Given User logs in into applitools website
                When User clicks on View Statement
                Then Following transactions should be present "<txnName>" and "<txnAmount>"

                Examples:

                        | txnName            | txnAmount   |
                        | MailChimp Services | - 320 USD   |
                        | Shopify product    | + 17.99 USD |


