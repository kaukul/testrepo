Feature: API Testing scenarios for POST and GET methods.

    This feature file covers API Testing scenarios for a demo website
    Demo website - https://reqres.in/..

    Scenario: Validating API testing with GET method
        Given User prepares data for GET scenario
        When User send the request for GET
        Then User should receive the correct response for GET

    Scenario: Validating API testing with POST method
        Given User prepares data for POST scenario
        When User send the request for POST
        Then User should receive the correct response for POST