import { Given, And, Then, When, Before } from "cypress-cucumber-preprocessor/steps"

/**
 * Step definitions for Testing GET and POST scenarios 
 * 
 */

var requestUrlForGET
var requestUrlForPOST
var item
var firstName
var lastName
var email

Before(function () {
  cy.fixture("global_data/env_config").then(function (globalData) {
    Cypress.config('baseUrl', globalData.baseUrlForApi)
  })
})

Given(/^User prepares data for GET scenario$/, () => {
  cy.fixture("test_data/api_get_post_validation").then(function (localData) {
    firstName = localData.dataForGet.firstName
    lastName = localData.dataForGet.lastName
    email = localData.dataForGet.email
    requestUrlForGET = localData.dataForGet.requestUrlForGET
  })
});

When(/^User send the request for GET$/, () => {
  cy.request(requestUrlForGET).then((response) => {
    expect(response.status).to.eq(200)
  })
})

Then(/^User should receive the correct response for GET$/, () => {
  cy.request(requestUrlForGET).then((response) => {
    expect(response.body.data).to.have.property('first_name', firstName)
    expect(response.body.data).to.have.property('last_name', lastName)
    expect(response.body.data).to.have.property('email', email)
  })
})

Given(/^User prepares data for POST scenario$/, () => {
  cy.fixture("test_data/api_get_post_validation").then(function (localData) {
    requestUrlForPOST = localData.dataForPost.requestUrlForPOST
    item = localData.item
  })
});

When(/^User send the request for POST$/, () => {
  cy.request('POST', requestUrlForPOST, item).then((response) => {
    expect(response.status).to.eq(201)
  });
})

Then(/^User should receive the correct response for POST$/, () => {
  cy.request('POST', requestUrlForPOST, item).its('body').should('include', item).then((response) => {
    expect(response.name).to.eq(item.name)
    expect(response.job).to.eq(item.job)
    expect(response).to.have.property('createdAt').to.not.be.null
  });
});

