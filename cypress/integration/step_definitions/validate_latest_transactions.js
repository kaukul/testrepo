import { Given, And, Then, When, Before } from "cypress-cucumber-preprocessor/steps";
import HomePage from "../../support/components/app_demo_applitools/home_page/home_page.po";
import LoginPage from '../../support/components/app_demo_applitools/login_page/login_page.po'

/**
 * Step definations to validate latest Transaction in the application 
 *  
 */

const loginPage = new LoginPage()
const homePage = new HomePage()
var url
var username
var password

Before(function () {
    cy.fixture("global_data/env_config").then(function (globalData) {
        url = globalData.URL
    })

    cy.fixture("test_data/validate_latest_transactions").then(function (localData) {
        username = localData.username
        password = localData.password
    })
})

Given(/^User logs in into applitools website$/, () => {
    loginPage.visit(url)
    loginPage.setUsername(username)
    loginPage.setPassword(password)
    loginPage.clickSignIn()
    homePage.validateTitle()
});

When(/^User clicks on View Statement$/, () => {
    cy.contains('View Statement').click()
});

Then(/^Starbucks transaction should be present$/, () => {
    cy.contains('Starbucks coffee').should('be.visible')
});

When(/^User clicks on View Statement$/, () => {
    cy.contains('View Statement').click()

});

Then(/^Amazon transaction should be present$/, () => {
    cy.contains('Amazon').should('be.visible')
});

Then("Following transactions should be present {string} and {string}", (txnName, txnAmount) => {
    cy.contains(txnName).should('be.visible')
    cy.contains(txnAmount).should('be.visible')
});
